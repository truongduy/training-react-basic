import React, { Component } from 'react';
import { map, uniqueId, uniqBy } from 'lodash';

import dataListNews from '../../mockupData/listNews.js';
import './news.css';

class News extends Component {
  constructor(props) {
    super(props);
    this.state = {
      news: [],
      filterTypeNews: [],
      isLoading: false,
    };
  }

  componentDidMount() {
    // this.setState({ isLoading: true });
    // fetch('https://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=ad0e55943d1e45a8b91e3caccd2119f9')
    // .then(response => response.json())
    // .then(data => this.setState({ news: data.articles, isLoading: false }))
    this.setState({ news: dataListNews, isLoading: false, filterTypeNews: dataListNews })
  }

  handleClick = (type) => {
    let newNewsFilter = this.state.filterTypeNews;
    newNewsFilter = newNewsFilter.filter((item) => (item.type.search( type ) !== -1 ));
    this.setState({ news: newNewsFilter });
  }

  renderListNews = (data) => map(data, (item, index) => {
    let urlImage = "";
    item.urlToImage === null ? urlImage = "https://dvynr1wh82531.cloudfront.net/sites/default/files/default_images/noImg_2.jpg" : urlImage = item.urlToImage;
    if(index < 20) {
      return (
        <div key={uniqueId()} className="item-news">
          <a href={item.url} title={item.title}>
            <div className="thumbnail">
              <img src={urlImage} alt={item.title} />
            </div>
            <div className="title">{item.title}</div>
          </a>
        </div>
      )
    }
  })

  renderTabs = (data) => map(uniqBy(data, 'type'), (item, index) => {
    if(index < 20) {
      return (
        <li key={index}><button onClick={() => this.handleClick(item.type)}>{item.text}</button></li>
      )
    }
  })

  render() {
    const { news, isLoading } = this.state;
    if (isLoading) {
      return <p>Loading News ...</p>;
    }

    return (
      <div className="news-wrapper">
        <h1 className="title-head">News</h1>
        <div className="tabs-type-wrapper">
          <ul className="list-tabs clearfix">
            <li key="all">
              <button onClick={() => this.handleClick()}>All</button>
            </li>
            {this.renderTabs(dataListNews)}
          </ul>
        </div>
        <div className="list-news-wrapper clearfix">
          <div className="list-news clearfix">
            {this.renderListNews(news)}
          </div>
        </div>
      </div>
    );
  }
}

export default News;
