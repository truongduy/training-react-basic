import React, { Component } from 'react';
import { map } from 'lodash';

import './style.css';
import logo from '../../logo.svg';

class Header extends Component {
  constructor() {
    super();

    this.state = {
      popupVisible: false
    }
  }

  handleClick() {
    if (!this.state.popupVisible) {
      console.log(this.state.popupVisible);
    }
    console.log(this.state.popupVisible);

    this.setState({
       popupVisible: !this.state.popupVisible,
    });
  }

  handleOutsideClick(e) {
    if (this.node.contains(e.target)) {
      return;
    }
    
    this.handleClick();
  }

  rennderNavi = (data) => {
    return map(data, (item, index) => (
      <li key={index}>
        <a href={item.link}>{item.text}</a>
      </li>
    ));
  }

  render() {
    const listNavi = this.props.listNavi;

    return (
      <header className="header clearfix">
        <div className="logo-wrapper">
          <a href="/">
            <img src={logo} className="logo" alt="logo" />
            <h1 className="title">React Sutrix</h1>
          </a>
        </div>
        <nav className="navigation" ref={node => { this.node = node; }}>
          <button className="btn-nav" onClick={() => this.handleClick()}>Menu</button>
          <ul className={`list-nav ${this.state.popupVisible ? 'open-mobile' : ''}`}>
            {this.rennderNavi(listNavi)}
          </ul>
        </nav>
      </header>
    )
  }
}

export default Header;
