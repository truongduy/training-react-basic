import React, { Component } from 'react';
import { map } from 'lodash';
import Slider from 'react-slick';

import './slick.css';
import './slider.css';

import NavLeftButton from '../../images/back.png';
import NavRightButton from '../../images/next.png';

class SlickSlider extends Component {
  renderSlider = (data) => map(data, (item, index) => (
    <div key={index} className="item-slide">
      <img src={item.src} alt={item.title} />
      <div className="content-slider">
        <div className="title">{item.title}</div>
        <div className="text">{item.text}</div>
      </div>
    </div>
  ));

  

  render() {
    const listSlider = this.props.listSlider;

    const RightNavButton = (props) => {
      return (
        <button onClick={props.onClick} type="button" className="slick-arrow slick-next">
          <img src={NavRightButton} alt="Next Button" />
        </button>
      )
    }
    const LeftNavButton = (props) => {
      return (
        <button onClick={props.onClick} type="button" className="slick-arrow slick-prev">
          <img src={NavLeftButton} alt="Pre Button" />
        </button>
      )
    }

    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      nextArrow: <RightNavButton/>,
      prevArrow: <LeftNavButton/>
    };

    return (
      <Slider className="slider-wrapper" {...settings}>
        {this.renderSlider(listSlider)}
      </Slider>
    )
  }
}

export default SlickSlider;
