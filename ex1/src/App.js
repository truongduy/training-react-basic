import React, { Component } from 'react';

import './App.css';
import Header from './Components/Header/index.js';
import SlickSlider from './Components/SlickSlider/index.js';
import News from './Components/News/index.js';
import navigationMenu from './mockupData/navigationMenu.js';
import listSlider from './mockupData/listSlider.js';


class App extends Component {
  render() {
    return (
      <div className="App">
        <Header listNavi={navigationMenu} />
        <SlickSlider listSlider={listSlider} />
        <div className="container-wrapper">
          <News />
        </div>
      </div>
    );
  }
}

export default App;
