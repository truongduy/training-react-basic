export default [  
  {  
    "author":null,
    "title":"Saudi Arabia eyes Indian crude market comeback, new downstream ventures",
    "description":null,
    "url":"https://www.platts.com/latest-news/oil/newdelhi/saudi-arabia-eyes-indian-crude-market-comeback-27917933",
    "urlToImage":null,
    "publishedAt":"2018-02-26T03:21:15Z",
    "type":"hitech",
    "text":"Hi-Tech"
  },
  {  
    "author":"Eric D. Lawrence",
    "title":"Report: Fiat Chrysler may drop diesels from car lineup",
    "description":"But it may keep them in trucks and SUVs",
    "url":"https://www.usatoday.com/story/money/cars/2018/02/25/report-fiat-chrysler-may-drop-diesels-car-lineup/371881002/",
    "urlToImage":"https://www.gannett-cdn.com/-mm-/6c515188ce1545921ea605ea21ee791f3d343d4c/c=1374-1050-2687-1792&r=x803&c=1600x800/local/-/media/2018/02/14/USATODAY/USATODAY/636542080592333299-AP18043696075227.jpg",
    "publishedAt":"2018-02-26T02:41:00Z",
    "type":"hitech",
    "text":"Hi-Tech"
  },
  {  
    "author":"Masayuki Kitano",
    "title":"Dollar steady, focus on Fed chief Powell's testimony",
    "description":"The dollar steadied on Monday, with investors looking to Federal Reserve Governor Jerome Powell's first congressional testimony for hints on the future pace of U.S. monetary tightening.",
    "url":"https://www.reuters.com/article/us-global-forex/dollar-steady-focus-on-fed-chief-powells-testimony-idUSKCN1GA02D",
    "urlToImage":"https://s2.reutersmedia.net/resources/r/?m=02&d=20180226&t=2&i=1235093874&w=1200&r=LYNXNPEE1P01H",
    "publishedAt":"2018-02-26T01:37:48Z",
    "type":"relax",
    "text":"Relax"
  },
  {  
    "author":"Brian Sozzi",
    "title":"General Electric Will Take a Big Profit Blow as It Restates Its Earnings",
    "description":"General Electric's latest annual report reveals how much it will cost to restate its earnings for 2016 and 2017....GE",
    "url":"https://www.thestreet.com/story/14499605/1/general-electric-will-take-a-big-profit-blow-as-it-restate-its-earnings.html",
    "urlToImage":"http://s.thestreet.com/files/tsc/v2008/photos/contrib/uploads/d8034392-fb99-11e7-ad76-1d025eace07d.png",
    "publishedAt":"2018-02-26T01:10:34Z",
    "type":"relax",
    "text":"Relax"
  },
  {  
    "author":"Noah Buhayar, Katherine Chiglinsky",
    "title":"Buffett Stresses Patience in a World Where Deals Look Expensive",
    "description":"Warren Buffett has a message for investors wondering when he’ll strike his next big deal: Be patient.",
    "url":"https://www.bloomberg.com/news/articles/2018-02-26/buffett-stresses-patience-in-a-world-where-deals-look-expensive",
    "urlToImage":"https://assets.bwbx.io/images/users/iqjWHBFdfxIU/iscTQzrdmbgk/v1/1200x800.jpg",
    "publishedAt":"2018-02-26T00:48:26Z",
    "type":"relax",
    "text":"Relax"
  },
  {  
    "author":"Sam Bourgi",
    "title":"Litecoin Recovers After Massive Slide as Charlie Lee Makes Bold Prediction",
    "description":"Litecoin rose on Monday, as prices clawed back huge losses from last week amid bullish remarks from the currency’s founder Charlie Lee. // -- Discuss and ask questions in our community on Workplace. LTC/USD Price Levels The Litecoin cryptocurrency reached a s…",
    "url":"https://hacked.com/litecoin-recovers-massive-slide-charlie-lee-makes-bold-prediction/",
    "urlToImage":"https://hacked.com/wp-content/uploads/2017/12/litecoin-1000x600.png",
    "publishedAt":"2018-02-26T00:47:03Z",
    "type":"relax",
    "text":"Relax"
  },
  {  
    "author":"Emily Rella",
    "title":"Yes, the rumors are true. Girl Scout Cookie coffee now exists.",
    "description":"The Thin Mints, Coconut Caramel, and Peanut Butter Cookie flavors will be available at select Dunkin' Donuts.",
    "url":"https://www.aol.com/article/lifestyle/2018/02/25/yes-the-rumors-are-true-girl-scout-cookie-coffee-now-exists/23370702/",
    "urlToImage":"https://o.aolcdn.com/images/dims3/GLOB/legacy_thumbnail/1028x675/format/jpg/quality/85/http%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2F244e5a2052cd08d7093c25fa815bdb0d%2F200003890%2F160975415.jpg",
    "publishedAt":"2018-02-26T00:15:00Z",
    "type":"sport",
    "text":"Sport"
  },
  {  
    "author":"Karen Gilchrist",
    "title":"These are the top cities for expat job opportunities",
    "description":"High salaries aren't necessarily the clincher.",
    "url":"https://www.cnbc.com/2018/02/23/top-cities-for-best-expat-job-opportunities.html",
    "urlToImage":"https://fm.cnbc.com/applications/cnbc.com/resources/img/editorial/2018/02/23/105024731-GettyImages-687774095_1.1910x1000.jpg",
    "publishedAt":"2018-02-26T00:01:00Z",
    "type":"sport",
    "text":"Sport"
  },
  {  
    "author":null,
    "title":"Toyota and Hyundai recall 110000 vehicles",
    "description":"No local vehicles have been impacted by the recall.",
    "url":"http://www.nzherald.co.nz/business/news/article.cfm?c_id=3&objectid=12002100",
    "urlToImage":"http://www.nzherald.co.nz/resizer/gTManJYMpMKvHfEhzND0EXT3Fcc=/1200x0/smart/filters:quality(70)/arc-anglerfish-syd-prod-nzme.s3.amazonaws.com/public/Y5633XXR3RHLTPLIGFWEFVSLSY.jpg",
    "publishedAt":"2018-02-25T22:51:29Z",
    "type":"sport",
    "text":"Sport"
  },
  {  
    "author":"Bill Ruthhart",
    "title":"Chicago, airlines nearing $8.5 billion deal to dramatically expand O'Hare",
    "description":"Mayor Rahm Emanuel and Chicago’s airline carriers are in the final stages of negotiating a blockbuster deal to dramatically expand O’Hare International Airport.",
    "url":"http://www.chicagotribune.com/news/local/politics/ct-met-city-hall-story-20180223-story.html",
    "urlToImage":"http://www.trbimg.com/img-5a935cae/turbine/ct-met-city-hall-story-20180223",
    "publishedAt":"2018-02-25T22:21:37Z",
    "type":"sport",
    "text":"Sport"
  },
  {  
    "author":null,
    "title":"Smart notes improving mobile internet speed",
    "description":"PLDT Inc.’s Smart Communications said investments in 4G had led to better mobile internet speed, citing crowdsourced data from OpenSignal.",
    "url":"https://business.inquirer.net/246548/smart-notes-improving-mobile-internet-speed",
    "urlToImage":"https://www.inquirer.net/wp-content/uploads/2016/10/new-inq-fb-def.jpg",
    "publishedAt":"2018-02-25T21:07:00Z",
    "type":"video",
    "text":"Video"
  },
  {  
    "author":"Jackie Wattles",
    "title":"More than a dozen businesses ran away from the NRA. How it went down",
    "description":"More than a dozen brands have chosen to cut ties with the NRA and vowed to stop offering discounts to members.",
    "url":"http://money.cnn.com/2018/02/25/news/companies/companies-abandoning-nra-list/index.html",
    "urlToImage":"http://i2.cdn.turner.com/money/dam/assets/180225082613-nra-1-780x439.jpg",
    "publishedAt":"2018-02-25T19:49:00Z",
    "type":"video",
    "text":"Video"
  },
  {  
    "author":"Jaden Urbi",
    "title":"Ranchers set to fight back against vegetarian 'fake meat'",
    "description":"Beef producers seek clarification on definitions as plant-based meat companies on the upswing",
    "url":"https://www.usatoday.com/story/money/business/2018/02/25/ranchers-set-fight-back-against-vegetarian-fake-meat/371378002/",
    "urlToImage":"https://www.gannett-cdn.com/-mm-/dafd2aa0a059fcff7d64a42517bd877878a8959b/c=0-2179-5147-5087&r=x1683&c=3200x1680/local/-/media/2017/03/21/USATODAY/USATODAY/636256952352319548-IF-Lifestyle-1.jpg",
    "publishedAt":"2018-02-25T19:26:00Z",
    "type":"video",
    "text":"Video"
  },
  {  
    "author":"Eyal Malinger, Beringea",
    "title":"Why VCs hate cryptocurrency investments — or why they should",
    "description":"While some VC firms have embraced ICOs (USV, Sequoia, DJF, and Seedcamp are some prime examples), the broader VC community has shown signs of fear and resentment towards ICOs. I have been following…",
    "url":"https://venturebeat.com/2018/02/25/why-vcs-hate-cryptocurrency-investments/",
    "urlToImage":"https://venturebeat.com/wp-content/uploads/2018/01/ico.jpg?fit=1200%2C850&strip=all",
    "publishedAt":"2018-02-25T18:25:00Z",
    "type":"video",
    "text":"Video"
  },
  {  
    "author":null,
    "title":"BSP governor recovering from tongue cancer",
    "description":"But for Bangko Sentral ng Pilipinas Governor Nestor Espenilla Jr, it is 'work as usual,' as he closely monitors peso volatility",
    "url":"https://www.rappler.com/business/196870-bsp-governor-espenilla-tongue-cancer",
    "urlToImage":"https://assets.rappler.com/612F469A6EA84F6BAE882D2B94A4B421/img/F505DC93194C4EA88D0240232A587B58/espi.jpg",
    "publishedAt":"2018-02-25T12:35:00Z",
    "type":"video",
    "text":"Video"
  },
  {  
    "author":"Mark Price",
    "title":"Latest on BB&T outage that drove customers crazy. Many still angry.",
    "description":"BB&T says systems “have substantially recovered” after an outage kept millions of customers from accessing accounts.",
    "url":"http://www.charlotteobserver.com/news/local/article202050099.html",
    "urlToImage":"http://www.charlotteobserver.com/news/local/fl7gf0/picture202050309/alternates/LANDSCAPE_1140/Eeping%20Money%20Safe%20Q%26A",
    "publishedAt":"2018-02-25T12:11:03Z",
    "type":"video",
    "text":"Video"
  },
  {  
    "author":"",
    "title":"When should singles take Social Security?",
    "description":"The decision of when to start collecting Social Security benefits can have serious and lasting consequences for the financial security of individuals and their spouses. Even though a wrong decision",
    "url":"http://www.eagletribune.com/news/when-should-singles-take-social-security/article_6d2b5c27-a93f-53b0-bc2a-2bab6f549fcf.html",
    "urlToImage":"https://bloximages.chicago2.vip.townnews.com/eagletribune.com/content/tncms/custom/image/ae213140-df8c-11e7-b06d-b798580d75a5.jpg?_dc=1513118096",
    "publishedAt":"2018-02-25T06:05:23Z",
    "type":"video",
    "text":"Video"
  },
  {  
    "author":"Doug Criss, CNN",
    "title":"Leaving Las Vegas? Dump your pot at the airport first",
    "description":"\"Amnesty boxes\" lets travelers dispose of legal marijuana bought in Sin City before going through airport security.",
    "url":"https://www.cnn.com/2018/02/24/us/vegas-marijuana-box-trnd/index.html",
    "urlToImage":"https://cdn.cnn.com/cnnnext/dam/assets/180224114425-02-vegas-marijuana-box-super-tease.jpg",
    "publishedAt":"2018-02-24T20:09:20Z",
    "type":"video",
    "text":"Video"
  },
  {  
    "author":"Bloomberg",
    "title":"Citigroup Is Refunding $335 Million to Credit Card Customers",
    "description":"",
    "url":"http://fortune.com/2018/02/24/citibank-refund-credit-cards/",
    "urlToImage":"https://fortunedotcom.files.wordpress.com/2018/01/gettyimages-813886142.jpg",
    "publishedAt":"2018-02-24T13:00:25Z",
    "type":"video",
    "text":"Video"
  },
  {  
    "author":"Cydney Henderson",
    "title":"Is there an Instant Pot recall? Company says some products are melting",
    "description":"To know if yours is affected, check the batchcode.",
    "url":"https://www.jsonline.com/story/news/nation-now/2018/02/23/instant-pot-company-says-some-products-melting/368722002/",
    "urlToImage":"https://www.gannett-cdn.com/-mm-/34655bbf44a6e7a3356bfa1ae43543def8edcabe/c=0-209-960-751&r=x803&c=1600x800/local/-/media/2018/02/23/Phoenix/Phoenix/636549992362106718-27972203-1567223176658400-9108208151611697209-n.jpg",
    "publishedAt":"2018-02-23T22:10:48Z",
    "type":"video",
    "text":"Video"
  }
];
