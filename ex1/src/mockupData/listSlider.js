import Slide1 from '../images/slide-1.jpg';
import Slide2 from '../images/slide-2.jpg';
import Slide3 from '../images/slide-3.jpg';

export default [
  { src: Slide1, title: 'Proident nisi qui adipisicing et qui.', text: 'Ut ad proident culpa elit aliqua aliqua. Aliquip dolor amet culpa ullamco sunt minim dolor amet esse anim culpa.' },
  { src: Slide2, title: 'Ex proident exercitation ipsum culpa.', text: 'Aute anim nisi amet consequat in qui pariatur mollit sit velit ex tempor pariatur.' },
  { src: Slide3, title: 'Duis id ut labore irure dolore labore dolore velit sint adipisicing.', text: 'Aliqua cupidatat eiusmod ad nisi fugiat non. Culpa eiusmod id ad occaecat magna ad anim ad Lorem dolor ex ad.' }
]
